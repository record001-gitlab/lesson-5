let wrapper = document.querySelector(".wrapper");

// fetch then then )

fetch("https://jsonplaceholder.typicode.com/users")
  .then((res) => res.json())
  .then((data) => {
    data.forEach((element) => {
      wrapper.innerHTML += ` 
        <div class="card">
            <h2>${element.name}</h2>
            <p>${element.phone} </p>
            <p>${element.email}</p>
            <p>${element.website}</p>
        </div>`;
    });
  });

  // async function 

async function getData() {
  let prom = await fetch("https://jsonplaceholder.typicode.com/users");
  let response = await prom.json();

  console.log(response);
}

// getData();

console.log("Request data...")
setTimeout(() => {
  console.log("Preparing data...");

  const data = {
    server: "aws", 
    port: 2000,
    status: "pending"
  }

  setTimeout(() => {
    data.status = "success"
    console.log("Data recieved", data);
  }, 2000);
  
}, 2000);


